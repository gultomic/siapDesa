{{-- Pelayanan untuk halaman publik --}}
@extends('layouts.public')

@section('content')
<ul class="nav nav-middle d-flex flex-nowrap text-nowrap bg-light">

</ul>

<section class="container-fluid nav-section">
  @yield('layanan')
</section>
@endsection
