@extends('layouts.apbdesa')

@section('layanan')
<div class="row">
  <div class="col-lg-9">
          <table id="apbdesatable" class="table table-borderless table-data3">
            <thead>
              <tr>
                {{-- <th>ID</th> --}}
                <th>Tanggal Upload</th>
                <th>Kategori</th>
                <th>Nama File</th>
                <th>Unduh Berkas</th>
              </tr>
            </thead>
            <tbody>
                @if ($list != array())
                    @for ($i = 0; $i < count($list); $i++)
                        <tr>
                            <td>{{$list[$i]->created_at}}</td>
                            <td>{{$list[$i]->kategori}}</td>
                            <td>{{$list[$i]->name}}</td>
                            <td>
                                <a href="{{asset('storage/apbdesa_uploads/'.$list[$i]->file)}}" target="_blank" class="waves-effect waves-light btn green accent-4" >Unduh</a>                                
                            </td>                                                                                    
                        </tr>
                    @endfor
                @endif
            </tbody>
          </table>        
  </div>
</div>
@endsection
