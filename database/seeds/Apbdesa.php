<?php

use Illuminate\Database\Seeder;

class Apbdesa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('apbdesa')->truncate();

        DB::table('apbdesa')->insert([
            'name' => 'Test Dokument',
            'kategori' => 'dokument',
            'file' => 'test.pdf',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()            
        ]);
    }
}
